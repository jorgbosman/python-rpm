def test_init_log():
    """
    TODO: with logfile, test content of file
    TODO: with stdout, capture output
    TODO: info vs debug with extra output
    TODO: somehow, logging.basicConfig is not working within pytest
    """
    import logging
    from lib.init import init_log
    init_log(logging.DEBUG, None)


def test_init(tmp_path):
    """
    TODO: read variable from config
    TODO: debug option should send log line to stdout
    """

    from lib.init import init  # noqa
