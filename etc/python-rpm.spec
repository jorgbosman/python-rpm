Name:     python-rpm
Version:  1.0.0
Release:  1%{?dist}
Summary:  Dummy python package
License:  Proprietary
URL:      https://gitlab.com/jorgbosman/python-rpm
Source0:  %{name}-%{version}.tar.gz
Source1:  etc
Requires: platform-python

%global debug_package %{nil}
%define install_path /opt/%{name}

%description
Example package, containing a python daemon, in a virtual environment, deployable as rpm

%prep
%setup -q

%build
# create virtualenv with required packages
python3 -m venv venv
source venv/bin/activate
pip install -U pip setuptools wheel
pip install .
# create all __pycache__ dirs
python3 -m compileall %{name}

# remove all absolute paths to the build dir
sed -i "s|^#!%{_builddir}/%{name}-%{version}/venv/bin/python3|#!/usr/bin/env python3|g" venv/bin/[a-z]*
sed -i "s|%{_builddir}/%{name}-%{version}/venv|%{install_path}/venv|g" venv/bin/[a-z]*

%install
mkdir -p %{buildroot}%{install_path}/venv
cp -r venv %{buildroot}%{install_path}/

# keep python itself out of the package, to prevent conflicts with libs outside the venv
rm %{buildroot}%{install_path}/venv/bin/python %{buildroot}%{install_path}/venv/bin/python3

install -m 0644 -D %{SOURCE1}/../README.md %{buildroot}%{install_path}/README.md
install -m 0644 -D %{SOURCE1}/../VERSION %{buildroot}%{install_path}/VERSION
install -m 0644 -D %{SOURCE1}/%{name}.conf %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf
install -m 0644 -D %{SOURCE1}/%{name}.service %{buildroot}%{_sysconfdir}/systemd/system/%{name}.service

%files
%dir /opt/%{name}
%dir /opt/%{name}/venv
/opt/%{name}/README.md
/opt/%{name}/VERSION
/opt/%{name}/venv/*
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%{_sysconfdir}/systemd/system/%{name}.service

%post
# update virtualenv, now with hardcoded paths to its current location instead of the build location
# (this fixes hardcoded path in bin/activate and links to the python3 version of the OS)
python3 -m venv %{install_path}/venv

# create log and data dirs
getent passwd pythonrpm >/dev/null 2>&1 || useradd --system pythonrpm
getent group pythonrpm >/dev/null 2>&1 || groupadd --system pythonrpm
install -d -o pythonrpm -g pythonrpm /var/log/%{name}

## setup database
## (does nothing if db+user+schema already exist)
#su - postgres -c psql < /opt/%{name}/bin/create_db.sql
#su - you -c "( . /opt/%{name}/venv/bin/activate ; /opt/%{name}/bin/create_schema.py -c /etc/%{name}/yourpkg.conf )"

# create and start daemons
systemctl daemon-reload
systemctl enable %{name}
systemctl start %{name}

%preun
systemctl stop %{name}

%postun
systemctl daemon-reload
getent passwd pythonrpm >/dev/null 2>&1 && userdel pythonrpm
getent group pythonrpm >/dev/null 2>&1 && groupdel pythonrpm

# venv/bin/python remains, which is created by the "python -m venv venv" command
rm -rf %{install_path} >/dev/null 2>&1
