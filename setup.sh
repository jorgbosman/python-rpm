#!/bin/bash -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
rm -rf $DIR/venv 2>/dev/null

python3 -m venv $DIR/venv
source $DIR/venv/bin/activate

# update self
pip install -U pip setuptools wheel

# for building the package
pip install tox

# dependencies of this app
pip install -e $DIR
