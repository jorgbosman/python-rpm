from setuptools import setup
from setuptools import find_namespace_packages

name = 'python-rpm'
description = 'Example package, containing a python daemon, in a virtual environment, deployable as rpm'
version = open('VERSION').read().strip()
readme = open('README.md').read().strip()

setup(
    name=name,
    version=version,
    description=description,
    long_description=readme,
    author='Jorg Bosman',
    author_email='jorg@bosman.tv',
    license='MIT License',
    include_package_data=True,
    package_dir={'': 'src'},
    packages=find_namespace_packages(where='src'),
    install_requires=[
        'requests',
    ],
    extras_require={
        'tests': [
            'flake8',
            'flake8-bugbear',
            'flake8-import-order',
            'flake8-typing-imports',
            'pep8-naming',
            'pip',
            'pytest',
            'pytest-cov',
            'setuptools',
            'tox',
        ],
    },
    entry_points={
        'console_scripts':
            (
                'python-rpm = python_rpm:main',
            )
    }
)
