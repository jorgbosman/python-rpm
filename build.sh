#!/bin/bash -x
#
# Build rpm package

PACKAGE_NAME=python-rpm
VERSION="1.0.0"
REVISION="1"
VERSION_FOR_RPM=${VERSION//-/_}
PACKAGE_VERSIONED=${PACKAGE_NAME}-${VERSION_FOR_RPM}

# full dir of this package
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMP=$DIR/tmp_package

# setup temp dirs
rm -rf $TMP
mkdir -p $TMP/etc
mkdir -p $TMP/rpmbuild
mkdir -p $TMP/$PACKAGE_VERSIONED

# put all relevant files in place
cp $DIR/etc/$PACKAGE_NAME.spec $TMP/$PACKAGE_VERSIONED/

# update spec file
sed -i "s/^Version:.*$/Version:\t${VERSION_FOR_RPM}/g" $TMP/$PACKAGE_VERSIONED/$PACKAGE_NAME.spec
sed -i "s/^Release:.*$/Release:\t${REVISION}/g" $TMP/$PACKAGE_VERSIONED/$PACKAGE_NAME.spec

# copy files in place
cp -rp $DIR/src $TMP/$PACKAGE_VERSIONED/
cp -p $DIR/etc/*.conf $TMP/etc/
cp -p $DIR/etc/*.service $TMP/etc/
cp -p $DIR/MANIFEST.in $DIR/setup.py $DIR/VERSION $DIR/PACKAGE $DIR/README.md $TMP/$PACKAGE_VERSIONED/
cp -p $DIR/VERSION $DIR/README.md $TMP/

TARFILE="$TMP/$PACKAGE_VERSIONED.tar.gz"
tar -C $TMP -cvzf $TARFILE $PACKAGE_VERSIONED

## pgsql path required for gcc psycopg2 libs
#PATH=/usr/pgsql-9.5/bin:$PATH

## QA_RPATHS=0x0001 required because uwsgi contains hardcoded path to /usr/lib64, which it should not
#QA_RPATHS=0x0001 rpmbuild -ta $TARFILE --define="_topdir $TMP/rpmbuild"
rpmbuild -ta $TARFILE --define="_topdir $TMP/rpmbuild"
