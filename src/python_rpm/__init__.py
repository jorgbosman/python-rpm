import datetime
import logging
import time
import traceback

import requests	 # just to demo dependencies in setup.py

from lib.init import init


class Daemon(object):
    def __init__(self, setting=None):
        self.setting = setting
        logging.info('starting')

    def run(self):
        logging.debug('setting: %s', self.setting)  # do stuff
        time.sleep(.9)

    def shutdown(self):
        logging.info('stopping')  # cleanly stop stuff


def main():
    args, cfg = init()

    daemon = Daemon(
    	setting = cfg.get('settings', 'setting')
    )

    while True:
        try:
            daemon.run()
            time.sleep(.1)
        except KeyboardInterrupt:
            break
        except Exception:
            logging.error(traceback.format_exc())
            time.sleep(3)

    daemon.shutdown()
