"""
Standard initializiation functions
"""

# TODO handle warnings.warning()
# TODO handle logging.shutdown()

import logging
import os
import sys
from argparse import ArgumentParser
from configparser import ConfigParser


def init_log(log_level, log_filename):
    """Setup anything logging related."""
    if log_level == 'DEBUG':
        line_format = '%(asctime)s.%(msecs)03d %(levelname)s [%(process)d:%(filename)s:%(lineno)d] %(message)s'
    else:
        line_format = '%(asctime)s.%(msecs)03d %(levelname)s %(message)s'

    if log_filename is None:
        logging.basicConfig(
            format=line_format,
            datefmt='%Y-%m-%d %H:%M:%S',
            stream=sys.stdout,
            level=log_level
        )
    else:
        if not os.path.exists(log_filename):
            head_tail = os.path.split(log_filename)
            if not os.path.exists(head_tail[0]):
                os.mkdir(head_tail[0])
        logging.basicConfig(
            format=line_format,
            datefmt='%Y-%m-%d %H:%M:%S',
            filename=log_filename,
            level=log_level
        )

    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def init():
    """Parse arguments and load config"""
    parser = ArgumentParser()
    parser.add_argument('-d', '--debug', action='store_true', help='Enable debug level and send logs to stdout')
    parser.add_argument('-c', '--config', help='Configuration file')
    args = parser.parse_args()

    if args.config is None or not os.path.exists(args.config):
        print('ERROR: Config file not found')
        print()
        parser.print_help()
        sys.exit(1)
    cfg = ConfigParser(delimiters=('=',))
    cfg.read(args.config)

    if args.debug:
        log_level = 'DEBUG'
        log_filename = None
    else:
        log_level = cfg.get('python-rpm', 'log_level', fallback='INFO')
        log_filename = cfg.get('python-rpm', 'log_filename', fallback=None)
    init_log(log_level, log_filename)

    return args, cfg
